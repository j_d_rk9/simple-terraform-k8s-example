#!/bin/bash

function check_os_and_kind() {
  # Check OS type
  if [[ $(uname) == Darwin ]]; then
    os="macOS"
  else
    # Check for common Linux distros package managers
    if command -v apt &> /dev/null; then
      os="Debian/Ubuntu"
      install_command="apt-get install -y"
    elif command -v yum &> /dev/null; then
      os="RHEL/CentOS/Alma/Rocky"
      install_command="yum install -y"
    elif command -v dnf &> /dev/null; then
      os="Fedora"
      install_command="dnf install -y"
    else
      os="Unknown Linux Distro"
    fi
  fi

  # Check for kind installation
  if command -v kind &> /dev/null; then
    kind_installed="true"
  else
    kind_installed="false"
    # Install kind if not found
    echo "kind not found. Installing..."
    if [[ "$os" == "macOS" ]]; then
      curl -Lo ./kind https://kind.sigs.k8s.io/dl/latest/kind-darwin-amd64
      chmod +x ./kind
      mv ./kind /usr/local/bin/kind
    else
      if [[ "$os" == "Unknown Linux Distro" ]]; then
        echo "Unsupported Linux distribution. Please install kind manually."
        return 1
      fi
      # Check for sudo and attempt installation (prompts for password)
      if ! command -v sudo &> /dev/null; then
        echo "This script requires sudo to install kind. Please run with sudo privileges."
        return 1  # Exit function with non-zero code to indicate error
      fi
      sudo -E $install_command kind
    fi
  fi

  # Print information
  echo "OS: $os"
  echo "kind installed: $kind_installed"
}

# Call the function to check OS and kind installation
check_os_and_kind

if [[ $? -ne 0 ]]; then
  echo "Failed to ensure kind is installed. Exiting..."
  exit 1
fi

# kind deploy
kind create cluster --name terraform-kubernetes-demo --config kind-config.yaml

# Verify existence
kind get clusters

# Point at cluster
kubectl cluster-info --context kind-terraform-kubernetes-demo

# Pull terraform variables and populate tf.vars file
kubectl config view --minify --flatten --context=kind-terraform-kubernetes-demo > info.context

# Prompt for confirmation before modifying terraform.tfvars
echo "This script will modify terraform.tfvars. Are you sure? (y/N)"
read -r confirmation

if [[ "$confirmation" =~ ^[Yy]$ ]]; then
  # Create terraform.tfvars with header
  echo "# tf vars" > terraform.tfvars

  # Extract and copy specific context data using grep
  # (adjust grep patterns for your specific data format)
  cert_authority=$(grep -Po '(?<=certificate-authority-data: ).*' info.context | tr -d '\r')
  server_value=$(grep -Po '(?<=server: ).*' info.context | tr -d '\r')
  client_cert=$(grep -Po '(?<=client-certificate-data: ).*' info.context | tr -d '\r')
  client_key=$(grep -Po '(?<=client-key-data: ).*' info.context | tr -d '\r')

  # Write extracted data as variables to terraform.tfvars
  echo "cert_authority = \"$cert_authority\"" >> terraform.tfvars
  echo "server_value = \"$server_value\"" >> terraform.tfvars
  echo "client_cert = \"$client_cert\"" >> terraform.tfvars
  echo "client_key = \"$client_key\"" >> terraform.tfvars

  echo "Context data copied to terraform.tfvars."
  terraform plan
  terraform apply
else
  echo "Aborting..."
fi
