resource "kubernetes_deployment" "nginx" {
  metadata {
    name = "long-live-the-bat"
    labels = {
      App = "longlivethebat"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        App = "entrypoint1"
      }
    }
    template {
      metadata {
        labels = {
          App = "entrypoint2"
        }
      }
      spec {
        container {
          image = "nginx:latest"
          name  = "entrypoint1"

          port {
            container_port = 80
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}
